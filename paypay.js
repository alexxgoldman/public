$(document).ready(function () {

    /*if (window.showAds) {
        document.getElementById('status').innerHTML = 'No adblocker detected';
    } else {
        document.getElementById('status').innerHTML = 'You have Adblocker!';
    }*/

    var minimum = 0.05;
    var wow = new WOW(); wow.init();

    // ПОЯВЛЕНИЕ ФРЕЙМА С САЙТОМ ЗА ЗАДАННОЕ ВРЕМЯ
    $('#iframe_view').fadeIn(2000);


    // ПОКАЗ ОКОН В ЗАВИСИМОСТИ ОТ ВЫБОРА ЕСТЬ ЛИ БАННЕР
    $('#do_you_have_banner').change(function() {
        var banner_have_result = $('#do_you_have_banner :selected').val();

        if (banner_have_result === 'i_have_banner')
        {
            $('#banner-logo').fadeIn(1500).css('background', 'red').animate({'background' : 'white'}, 1000);
            $('#label_url_banner').hide();
            $('#banner-img_url').hide();
            $('#title_text').hide();
        }
        else if (banner_have_result === 'i_have_no_banner')
        {
            $('#banner-logo').hide();
            $('#label_url_banner').hide();
            $('#banner-img_url').hide();
            $('#title_text').fadeIn(1500);
        }
        else if (banner_have_result === 'i_have_url_banner')
        {

            $('#banner-img_url').fadeIn(1500);
            // $('#banner-img_url').animate({'width':'100%'}, 400).animate({'width':'40%'}, 400).animate({'width':'100%'}, 400).animate({'width':'40%'}, 400);
            $('#banner-img_url').css('border', '3px solid red');
            // НОВОЕ ПОЛЕ ВВОДА (РАМКА) ПОДСВЕЧИВАЕТСЯ КРАСНЫМ И ЧЕРЕЗ 3 СЕК ПОДСВЕТКА ИСЧЕЗАЕТ
            setTimeout(function () {
                $('#banner-img_url').css('border', '1px solid gray');
            }, 3000);
            $('#label_url_banner').fadeIn(1500);
            $('#title_text').hide();
            $('#banner-logo').hide();
        }
        else return false;

    });

    //КОЛИЧЕСТВО ОСТАВШИХСЯ СИМВОЛОВ ЕСЛИ ПОЛЬЗОВАТЕЛЬ ВЫБРАЛ ТЕКСТОВЫЙ ВАРИАНТ БАННЕРА
    // "title"
    $('#banner_title').bind("input", function() {
        var length = $('#banner-title').val().length;
        var rest = MAX_BANNER_TITLE - length;
        var useless = 0 - rest;
        $('#title_left').html("<h5>Осталось " + rest + " симв</h5>");
        if (rest < 0)
        {
            $('#title_left').html("<h5 style=\"color:red\"><strong>Текста достаточно! Лишних символов " + useless + "</strong></h5>");
            $('#banner-upload').attr('disabled', 'disabled');
        }
        if (rest > 0)
        {
            $('#banner-upload').removeAttr('disabled');

        }
    });
    // "text"
    $('#banner-text').bind("input", function() {
        var length = $('#banner-text').val().length;
        var rest = MAX_BANNER_TEXT - length;
        var useless = 0 - rest;
        $('#text_left').html("<h5>Осталось " + rest + " симв</h5>");
        if (rest < 0)
        {
            $('#text_left').html("<h5 style=\"color:red\"><strong>Текста достаточно! Лишних символов " + useless + "</strong></h5>");
            $('#banner-upload').attr('disabled', 'disabled');
        }
        if (rest > 0)
        {
            $('#banner-upload').removeAttr('disabled');

        }
    });


    // ПРИ НАЖАТИИ НА ПРЕДЛАГАЕМУЮ ЦЕНУ ВСТАВЛЯЕТ ЕЁ В ПОЛЕ
    $('#cost_01').click(function(){
        $('#banner-cost').attr('value','0.1');
    });
    $('#cost_02').click(function(){
        $('#banner-cost').attr('value','0.2');
    });
    $('#cost_03').click(function(){
        $('#banner-cost').attr('value','0.3');
    });
    $('#cost_max').click(function(){
        $('#banner-cost').attr('value', cost_max);
    });


    //ФУНКЦИЯ ВЫВОДА БАННЕРОВ ПООЧЕРЁДНО
    function showBanner() {
        var b = $('.banner-hidden:first');
        if (b.size()) {
            b.removeClass('banner-hidden');
            setTimeout(function() {
                showBanner();
            }, 1000);
        }
    }
    $(function() {
        showBanner();
        $('.banners').on('click', function() {
            setTimeout(function() {
                window.location.href = '/';
            }, 500);
        });
    });


    //ВЫВОД ДЕНЕГ
    $("#btn_payout_2").on('click', function(event) {
        event.preventDefault();
        var summ_output = $('#summ_output').val();
        var payeer_account_number = $('#payeer_account_number').val();
        $.ajax({
            url:"/payout",
            type: "GET",
            data: "summ_output="+summ_output+"&payeer_account_number="+payeer_account_number,
            dataType:"html",
            success: function (response)
            {
                if(response === 'success')
                {
                    $('#payout_form').hide();
                    $('#message_success').show(800);
                }
                else if (response === 'noMoreToday')
                {
                    $('#payout_form').hide();
                    $('#message_error_noMoreToday').show(800);
                }
                else if (response === 'moreThanYouHave')
                {
                    $('#payout_form').hide();
                    $('#message_error_moreThanYouHave').show(800);
                }
                else
                {
                    $('#payout_form').hide();
                    $('#message_error').show(800);
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
	


    // ПРИ КЛИКЕ МЫШИ НА БАЛАНС ПОЯВЛЯЕТСЯ КНОПКА "ВЫВЕСТИ"
    $('#btn_balance').on('click', function(){
        $('#payout_form').fadeIn(1000);
        $('#payin_form').hide();
        $('#earn_button').hide();
		$('#change_window').hide();
    });

    // ФОРМА ВЫВОДА ДЕНЕГ PAYEER
    $('#btn_payout_1').on('click', function(){

    });

    //ФОРМА ПОПОЛНЕНИЯ СЧЁТА ЧЕРЕЗ PAYEER
    $('#adv_balance_show').on('click', function () {
        $('#payin_form').fadeIn(1000);
        $('#payout_form').hide();
        $('#earn_button').hide();
		$('#change_window').hide();
    });
	
	


    // ПРИ НАЖАТИИ НА КНОПКУ "ПРОШЛІЙ БОНУС" ПОЯВЛЯЕТСЯ КНОПКА ПОЛУЧЕНИЯ БОНУСА
    $('#last_earn').on("click", function () {
        $('#earn_button').fadeIn(1000);
        $('#payin_form').hide();
        $('#payout_form').hide();
        $('#change_window').hide();
    });

    // ПРИ НАЖАТИИ НА РИСУНОК КНОПКИ ПОЛУЧЕНИЯ БОНУСА ОН УМЕНЬШАЕТСЯ В РАЗМЕРАХ ТИПА НАЖАЛИ
    $('#earn_button').on("click", function () {
        $('#earn_button').hide();
    });

    // ПРИ ВЫБОРЕ ВРЕМЕНИ ПРОСМОТРА САЙТА ВО ФРЕЙМЕ ПОСЛЕ ПЕРЕХОДА ПО БАННЕРУ: ЧЕМ БОЛЬШЕ ВРЕМЯ, ТЕМ ДОРОЖЕ min КЛИК
    $('#watch_time').change(function () {
        var sec = $('#banner-watch_time :selected').val();
        var min = 0.05;

        if (sec == 5)
        {
            minimum = (minimum).toFixed(2);
            $('#banner-cost').attr('min', minimum);
            $('#resume_cost').html(minimum);
        }
        else if (sec == 6)
        {
            minimum = (minimum + 0.01).toFixed(3);
            $('#banner-cost').attr('min', minimum);
            $('#resume_cost').html(minimum);
        }
        else if (sec == 7)
        {
            min = (min + 0.012).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 8)
        {
            min = (min + 0.014).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 9)
        {
            min = (min + 0.016).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 10)
        {
            min = (min + 0.018).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 11)
        {
            min = (min + 0.02).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 12)
        {
            min = (min + 0.022).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 13)
        {
            min = (min + 0.024).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 14)
        {
            min = (min + 0.026).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (sec == 15)
        {
            min = (min + 0.03).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }

        $('#banner-cost').val(min);
    });

   // ПРИ ВЫБОРЕ ПЕРИОДА ИНДИВИДУАЛЬНОГО ПОКАЗА БАННЕРА
    // изменил на +0,00 потому что оказываентся за это уже не берутся деньги 
    $('#banner-period_individual').change(function () {
        var period_individual = $('#period_individual :selected').val();
        var min = 0.05;
        if (period_individual == 1800)
        {
            min = (min).toFixed(2);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(minimum);
        }
        else if (period_individual == 3600)
        {
            min = (min + 0.00).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(minimum);
        }
        else if (period_individual == 7200)
        {
            min = (min + 0.00).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(minimum);
        }
        else if (period_individual == 14400)
        {
            min = (min + 0.00).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (period_individual == 43200)
        {
            min = (min + 0.00).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }
        else if (period_individual == 86400)
        {
            min = (min + 0.00).toFixed(3);
            $('#banner-cost').attr('min', min);
            $('#resume_cost').html(min);
        }

        $('#banner-cost').val(min);
    });

    $('#menu_adv_balance').on("click", function (e) {
        e.preventDefault();
        $('#payin_form').fadeIn(1000);
        $('#payout_form').hide();
        $('#earn_button').hide();
		$('#change_window').hide();
    });

    $('#menu_balance').on("click", function (e) {
        $('#payout_form').fadeIn(1000);
        $('#payin_form').hide();
		$('#earn_button').hide();
		$('#change_window').hide();
    });


	


}); // конец ready