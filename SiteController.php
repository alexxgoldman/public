<?php

namespace app\controllers;

use app\models\Autojob;
use app\models\AutojobComments;
use app\models\AutojobDone;
use app\models\Bonus;
use app\models\CompetitionCollection;
use app\models\Games;
use app\models\Messages;
use app\models\NewsComment;
use app\models\PollRefCompetition;
use app\models\Rating;
use app\models\Signup;
use app\models\ContactForm;
use app\models\Login;
use app\models\Sitelist;
use app\models\SitelistComments;
use app\models\SitelistRating;
use app\models\User;
use app\models\Click;
use app\models\CPayeer;
use app\models\Payout;
use app\models\Event;
use app\models\ClickBanner;
use app\models\Banner;
use app\models\Payin;
use app\models\Revenue;
use app\models\News;
use app\models\LoginForm;
use app\models\Sitereg;
use app\models\LinkSlot;
use app\classes\Request;
use app\classes\Session;
use app\classes\Tools;
use app\models\UserOnline;
use app\models\UserRefBalance;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\swiftmailer\Message;
use yii\web\Controller;
use yii\web\IdentityInterface;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\Pagination;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//  ================== АДМИНКА ======================== //

    public function actionAdmin(){
        if ($_SERVER['REMOTE_ADDR'] != '46.46.79.44') exit('У вас нет прав');

        $count_1 = User::find()->where(['id_ref' => 38])->count();
        $count_2 = User::find()->where(['id_ref' => 4])->count();
        $has_avatar = User::find()->where(['','avatar_url',null])->count();

        return $this->render('admin', compact('count_1', 'count_2', 'has_avatar'));
    }

//  ================== СТРАНИЦЫ ТЕСТИРОВАНИЯ ======================== //

    public function actionTest()
    {

        //User::accessDeniedByIP();

        if (empty(User::getLogin()))
        {
            $this->layout = 'isGuest';
        }

        $event = Event::find();



        $query = Event::find();
        $countQuery = clone $query;
        $pages = new Pagination(['defaultPageSize' => 3, 'totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset) // были сомнения: числом или вот так
        ->limit($pages->limit) // были сомнения: числом или вот так
        ->all();

        $banner = new Banner();
        $banners = Banner::getByAuthor(User::getLogin());


     /*   $refs_array = Yii::$app->db->createCommand("
                SELECT COUNT(id_ref) AS count, id_ref
                FROM user
                WHERE datetime BETWEEN ('2018-02-15 00:00:00') AND ('2018-02-17 23:59:59')
                GROUP BY id_ref
                ORDER BY count DESC
        ")->queryAll();*/


        /*$refs_array = Yii::$app->db->createCommand("
                SELECT SUM(value) AS sum, id_ref
                FROM bonus
                WHERE datetime BETWEEN ('".COMPETITION_BEGIN."') AND ('".COMPETITION_END."')
                AND (`id_ref` != 0)
                GROUP BY id_ref
                ORDER BY sum DESC
        ")->queryAll();*/

        //$refs_array = User::find()->limit(10)->orderBy('points_ref_1 desc')->all();

        $payout_array = Payout::find()->limit(50)->orderBy('time desc')->all();

        $url = $_SERVER['REQUEST_URI'];

        $payout_array = Payout::find()->limit(100)->all();

        $refs_array = User::find()->limit(20)->orderBy('points_ref_1 DESC')->all();
        $refs_earn_more_zero = User::find()->where(['>', 'points_ref_1', 0])->all();

        //$games_data = Games::getLogo();
		
		$linkslot = LinkSlot::find()->all();

        $user = User::find()->where(['id' => User::getUserId()])->one();

        $my_refs = User::getMyFirstRefs(10);

        $autojob_array = \app\models\Autojob::find()->all();

        $winners_array = CompetitionCollection::getWinners(1,3);

        return $this->render('test', compact('banners', 'banner', 'pages', 'models', 'event', 'url', 'refs_array',
            'payout_array', 'refs_earn_more_zero', 'games_data', 'linkslot', 'user', 'my_refs', 'autojob_array', 'winners_array'));
    }

    public function actionTest2()
    {
        $payoutTotal = Payout::getAll();

        return $this->render('test2', compact('payoutTotal'));
    }

    public function actionBannersMy2()
    {
        $banner = new Banner();
        $banners = Banner::getByAuthor(User::getLogin());
        return $this->render('banners-my-2', compact('banners', 'banner'));
    }

    public function actionCompetition(){


        $refs_array = Yii::$app->db->createCommand("
                SELECT SUM(value) AS sum, id_ref
                FROM bonus
                WHERE datetime BETWEEN ('".COMPETITION_BEGIN."') AND ('".COMPETITION_END."')
                AND (`id_ref` != 0)
                GROUP BY id_ref
                ORDER BY sum DESC
        ")->queryAll();

        return $this->render('competition', compact('refs_array'));
    }

//  ================== CRON обслуживание базы данных ======================== //

    public function actionCronDeleteEvent(){
        $time_to_delete_from = time() - 1*60*60;
        Yii::$app->db->createCommand("
            DELETE FROM `event`
            WHERE (`time` < $time_to_delete_from)
        ")->execute();
    }

    public function actionCronDeleteClickBanner(){
        $time_to_delete_from = time() - 24*3600;
        Yii::$app->db->createCommand("
            DELETE FROM `clickBanner`
            WHERE (`date` < $time_to_delete_from)
        ")->execute();
    }

//  ================== ОСНОВНЫЕ СТРАНИЦЫ ======================== //

    public function actionIndex()
    {
       /* if (empty(Yii::$app->user->identity->login))
        {
             $this->layout = 'isGuest';
        }*/

        if (Yii::$app->user->isGuest)
        {
            $time = time();
            $datetime = date('Y-m-d H:i:s');

            $model = new Signup();
            $login_model = new Login();



            $url = $_SERVER['REQUEST_URI'];

            $id_ref = Session::get('id_ref');

            if (!isset($id_ref)) {
                $refovod_id = substr($url, 2);
                Session::set('id_ref', $refovod_id);
            }

            if (Yii::$app->request->post('Login'))
            {
                $login_model->attributes = Yii::$app->request->post('Login');
                if ($login_model->validate())
                {
                    Yii::$app->user->login($login_model->getUser(),  60*60*24*30*365);
                    return $this->goHome();
                }
            }

            if (isset($_POST['Signup']))
            {
                $model->attributes = Yii::$app->request->post('Signup');

                if($model->validate() && $model->signup())
                {
                    return $this->goHome();
                }
            }

            return $this->render('index', compact('model', 'login_model', 'url', 'id'));
        }
        else
        {
            //добавляем пропущенный IP
            User::setIP();
            $login = User::getLogin();
            $id = (null != User::getUserId()) ? User::getUserId() : '';
            $earn_total_on_ref = User::getEarnTotalOnRef();
            $my_ref_qty = User::getReferralsQty();
            $pollYes = PollRefCompetition::getAnswersYesQty();

           /*$refs_array = Yii::$app->db->createCommand("
                SELECT SUM(value) AS sum, id_ref
                FROM bonus
                WHERE datetime BETWEEN ('".COMPETITION_BEGIN."') AND ('".COMPETITION_END."')
                AND (`id_ref` != 0)
                GROUP BY id_ref
                ORDER BY sum DESC
        ")->queryAll();*/
            $refs_array = User::find()->where(['>', 'points_ref_1', 0])->orderBy('points_ref_1 desc')->all();
                
            $payout_array = Payout::find()->limit(10)->orderBy('time desc')->all();
            $payin_array = Payin::find()->limit(10)->orderBy('datetime desc')->all();

            $user = User::find()->where(['id' => User::getUserId()])->one();
            $news = News::getAllByCategory('news');
            $news_main = News::getAllByCategoryLimit('news', 3);

            //$my_refs = User::getMyRefs();
            $my_refs = User::getMyFirstRefs(10);

            $autojob_exists = Autojob::find()->where(['id_author' => $id])->exists();

            $event_last50 = Event::getLast50();

            $refs_array_comp = Yii::$app->db->createCommand("
                SELECT SUM(value) AS sum, id_ref
                FROM bonus
                WHERE datetime BETWEEN ('".COMPETITION_BEGIN."') AND ('".COMPETITION_END."')
                AND (`id_ref` != 0)
                GROUP BY id_ref
                ORDER BY sum DESC
        ")->queryAll();

            $winners_array = CompetitionCollection::getWinners(1,3);

            return $this->render('u', compact('login', 'balance', 'adv_balance', 'diff', 'rest', 'models',
                'pages', 'event_array', 'id', 'earn_total_on_ref', 'my_ref_qty', 'pollYes', 'refs_array', 'payout_array',
                'payin_array', 'user', 'news_main', 'my_refs', 'autojob_exists', 'event_last50', 'refs_array_comp', 'winners_array'));
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }

        return $this->render('about');
    }

    public function actionFaq()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }

        return $this->render('faq');
    }

    public function actionPolicy()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }

        return $this->render('policy');
    }

    public function actionRules()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }

        return $this->render('rules');
    }

    public function actionSignup()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }


        $time = time();
        $datetime = date('Y-m-d H:i:s');

        $model = new Signup();

        if (isset($_POST['Signup']))
        {
            $model->attributes = Yii::$app->request->post('Signup');

            if (User::checkExistsIP()) exit ('<center><h1>You already have an account from this IP</h1></center>');

            if($model->validate() && $model->signup())
            {
                Session::set('reg_success', "Поздравляем с успешной регистрацией!");
                return $this->goHome();
            }
        }

        return $this->render('signup', compact('model'));
    }

    public function actionLogin()
    {
        if (empty(Yii::$app->user->identity->login))
        {
            $this->layout = 'isGuest';
        }

        $login_model = new Login();
        $time = time();
        $datetime = date('Y-m-d H:i:s');


        if (Yii::$app->request->post('Login'))
        {
           $login_model->attributes = Yii::$app->request->post('Login');
           if ($login_model->validate())
           {
               Yii::$app->user->login($login_model->getUser(),  COOKIE_TIME_LOGIN);
               return $this->goHome();
           }

        }

        $login = 'Ещё один человек';
        $message = "Ещё один человек <strong>вошёл</strong> на сайт!";

         Yii::$app->db->createCommand(
            "INSERT INTO `event` (`login`, `message`, `time`, `datetime`) 
                                          VALUES (:login, :message, :time, :datetime)")
            ->bindParam(':login', $login)
            ->bindParam(':message', $message)
            ->bindParam(':time', $time)
            ->bindParam(':datetime', $datetime)
            ->execute(); 

        return $this->render('login', compact('login_model'));
    }

    public function actionSiteOff(){
        return $this->render('site-off');
    }

	public function actionReferrals()
	{
		return $this->render('referrals');
	}

    public function actionGetref()
    {
        return $this->render('getref');
    }


    /*------------------ОСНОВНОЕ-------------------*/

    public function actionRotatorPay()
    {
        $login = User::getLogin();
        $time = time();
        $datetime = date('Y-m-d H:i:s');
        $click = Click::find()->where(['login' => $login])->max('time');
        $last_click_exists = Click::find()->where(['login' => $login])->exists();  // клацал ли юзер хоть когда-то
        $diff = time() - $click; // время ВООБЩЕ после последнего клика

        if ($diff < BONUS_PERIOD)
        {
            echo '0';
            exit();
        }
        else
        {
            $lastBonusTime = date('H:i:s');
            $bonus = Request::post('bonus');
            Session::set('lastBonus', "Вы только что получили " . $bonus. " балла(-ов)!");
            Session::set('lastBonusTime', $lastBonusTime);

            $user_get_refovod_id = User::getRefovodId();
            User::payHisRefovodPoints($bonus);

            User::updatePoints($login, $bonus);
            Click::add($login, $bonus);
            Revenue::add($login, $bonus);
            $rotator_cost_to_pay_for_click = $bonus * USER_COMISSION;
            Event::add($login, "Пользователь <strong>$login</strong> заработал на клике по бонуснику <span style='color:darkred; font-weight: bold'> $rotator_cost_to_pay_for_click </span> баллов");
            //заполняем упущенный IP
            User::setIP();
            echo '1';
        }


    }

    public function actionPayeerFormIn()
    {
        return $this->render('payeer-form-in');
    }

    public function actionPayeerFormOut()
    {
        return $this->render('payeer-form-out');
    }

    public function actionPayment()
    {
        // было $orderid = '1'; // принимать реальный номер нового заказа через $_POST
        $orderid = '1'; // принимать реальный номер нового заказа через $_POST
        $amount = xss(Request::post('m_amount'));  // тут было с ошибкой mount. Исправил сам
        $curr = xss(Request::post('m_curr'));
        $desc = 'Тест';

        $m_shop = M_SHOP;      // смотрим Аккаунт -> Подключение на сайт -> Ручная настройка магазина
        $m_orderid = 1;
        $m_amount = number_format($amount, 2, '.', '');  // тут изначально почему-то стояло 100. Но должно стоять $amount, потому что $_POST['amount'] который приходит с формы платежа
        $m_curr = 'RUB';
        $m_desc = base64_encode('Тест');
        $m_key = M_KEY; // секретные коды ставил одинаковые, что в магазин, что на апи, чтоб не запутаться

        $arHash = array(
            $m_shop,
            $m_orderid,
            $m_amount,
            $m_curr,
            $m_desc,
            $m_key
        );
        $sign = strtoupper(hash('sha256', implode(':', $arHash)));

        return $this->render('payment', compact('orderid', 'amount', 'curr', 'desc', 'm_shop', 'm_orderid', 'm_amount', 'm_curr', 'm_desc', 'm_key', 'sign'));
    }

    // только тут идёт запись о пополнениях
    // +20% при первом пополнении здесь же
    public function actionSuccess()
    {

//        $m_operation_id = $_GET['m_operation_id'];
        $m_operation_ps = xss($_GET['m_operation_ps']);
//        $m_operation_date = $_GET['m_operation_date'];
        $m_operation_pay_date = xss($_GET['m_operation_pay_date']);
        $m_operation_id = xss($_GET['m_operation_id']);
        $m_shop = xss($_GET['m_shop']);
        $m_orderid = xss($_GET['m_orderid']);
        $cash = xss($_GET['m_amount']); // т.е. $m_amount
        $m_curr = xss($_GET['m_curr']);
        $m_desc = xss($_GET['m_desc']);
        $m_status = xss($_GET['m_status']);
        $m_sign = xss($_GET['m_sign']);

        $login = User::getLogin();
        $date = time();
        $datetime = date('Y-m-d H:i:s');

        Payin::add($login, $cash, $m_operation_id, $m_status, $m_operation_pay_date, $m_curr, $m_sign, $date, $datetime);

        if (Payin::isFirstPayment_byLogin($login))
        { // при первом пополнении счёта дарим PAYMENT_FIRST % в подарок
            $cash_plus_percent = $cash + $cash * (PAYMENT_FIRST / 100);
            User::changeAdvBalance($login, $cash_plus_percent);
        }
        else
        {
            User::changeAdvBalance($login, $cash);
        }

        return $this->render('success', compact('login', 'cash', 'm_operation_id', 'm_status', 'm_operation_pay_date', 'm_curr', 'm_sign', 'date', 'datetime'));
    }

    public function actionPayout()
    {
		$login = User::getLogin();
        $now = time();
        $datetime = date('Y-m-d');
        $summ_output = Request::get('summ_output');
		//$payeer_account_number = Request::get('payeer_account_number');
		//$payeer_account_number = $login;

        //сколько пользователь вывел за сегодня
        $payoutToday = Payout::getTotalTodayByLogin($login);

        //делаем важную проверку:  не многовато ли запросил пользователь? Может у него на счету 1 руб, а выводит 1000?
        $balance = User::getBalance(); // достаём его баланс из БД
        if ( $summ_output > $balance ) { // если выводит больше чем на балансе, то сразу же выкидываем
            echo 'moreThanYouHave';
//            header("Location: fail-max-limit");
           // exit("Вы пытаетесь вывести больше средств, чем располагаете...");
            exit();
        }
        elseif ($payoutToday >= PAYOUT_PER_DAY)
        {
            echo 'noMoreToday';
            //exit("За день можно вывести не более $payoutPerDay руб");
            exit();
        }
        else
        {
			//$payeer_account_number = Request::get('payeer_account_number');
			$payeer_account_number = $login;
            $accountNumber = ACCOUNT_NUMBER;
            $apiId = API_ID; // "Массовые выплаты" -> ID (напротив Имя Пользователя, не спутай с ID магазина в "Настройках мерчанта")
            $apiKey = API_KEY; // ключ ставлю везде одинаковый
            $payeer = new CPayeer($accountNumber, $apiId, $apiKey);
            if ($payeer->isAuth()) {
                $initOutput = $payeer->initOutput(array(
                    'ps' => '1136053',
                    //'sumIn' => 1,
                    'curIn' => 'RUB',
                    'sumOut' => $summ_output,
                    'curOut' => 'RUB',
                    'param_ACCOUNT_NUMBER' => $payeer_account_number
                ));

                if ($initOutput)
                {
                    $historyId = $payeer->output();
                    if ($historyId > 0)
                    {
						$login = User::getLogin();
						User::changeBalance($login, -$summ_output);
                        $message = "<img src=\"/web/img/koshelek.ico.png\" width=\"20\" height=\"20\"/> &nbsp; $login <b> вывел(а) $summ_output руб </b>";
                        Event::add($login, $message);
                        Payout::add($login, $summ_output);
                        echo 'success';
                    }
                    else
                    {
                       echo '<pre>1'.print_r($payeer->getErrors(), true).'</pre>';
                        //echo 'error_1';
                    }
                }
                else
                {
                    echo '<pre>2'.print_r($payeer->getErrors(), true).'</pre>'; // добавил аж 13.02.2017, когда Payeer внезапно перестал выводить деньги
                    // в боевом режиме нужно убирать или заменить на страницу описывающую проблему
                }

            }
            else
            {
                echo '<pre>3'.print_r($payeer->getErrors(), true).'</pre>';
            }

        }


        //return $this->render('payout', compact('accountNumber', 'apiId', 'apiKey', 'payeer', 'summ_output'));
    }

    public function actionFailMaxLimit()
    {
        return $this->render('fail-max-limit');
    }

    public function actionBanners_()
    {

        $model = new Banner();

        // задаём переменные-помощники

        if ($model->load(Yii::$app->request->post()))
        {
            $login = User::getLogin();
            $banner_title = xss($_POST["Banner"]["title"]);
            $banner_text = xss($_POST["Banner"]["text"]);
            $banner_img_url = xss($_POST["Banner"]["img_url"]);
            $model->author = User::getLogin(); // получаем логин для внесения в БД
            $model->date = time();
            $model->datetime = date('Y-m-d H:i:s');
            $imageName = Tools::getRandomName();
            $model->logo = UploadedFile::getInstance($model, 'logo'); // получаем объект файла (баннер)

            if ( empty($banner_text) && empty($banner_title) ) {
                $model->img = 'uploads/' . $imageName . '.' . $model->logo->extension; // сохраняем адрес объекта (баннера) в
                $imageName = Tools::getRandomName();
                $model->logo->saveAs('uploads/' . $imageName . '.' . $model->logo->extension); // физически переносим объект (баннер) в папку

            }
            else if ( !empty($banner_text) ) {
                $imageRandomName = Tools::getRandomName();
                $model->img = 'uploads/' . $imageRandomName; // сохраняем адрес объекта (баннера) в

            }
            else if ( !empty($banner_img_url) ) {
                $model->img_url = $banner_img_url;

            }

            $model->save();

//
//            $model->url = xss($_POST["Banner"]["url"]); // ЭКВИВАЛЕНТ 'value' = 'http://www.' ЧТОБЫ В ПОЛЕ УЖЕ БЫЛО ВВЕДЕНО НАЧАЛО
//            $model->cost = 0.08;
//            $model->time = time();
//            $model->status = 1;
//            $model->datetime = date('Y-m-d H:m:s');


//
//            $now = time();
//            $datetime = date('Y-m-d H:i:s');
//            $message = "Пользователь $login <b>добавил новый баннер</b>";
//            Yii::$app->db->createCommand("INSERT INTO `event` (`message`, `date`, `datetime`) VALUES (:message, :date, :datetime)")
//                ->bindParam(':message', $message)
//                ->bindParam(':date', $now)
//                ->bindParam(':datetime', $datetime)
//                ->execute();

//            Yii::$app->session->setFlash('uploaded_ok', "Ваш баннер успешно загружен! Посмотреть <a href='".Url::to(['banner-my'])."' style='text-decoration: none; border-bottom:1px dotted'>мои баннеры</a>");
        }



        return $this->render('banners', compact('model')); // всю эту информацию перекидываем на страницу upload.php
    }

    //добавление нового баннера
    public function actionBanners()
    {

        $success = false;
        $model = new Banner();

        // задаём переменные-помощники

        if ($model->load(Yii::$app->request->post()))
        {
            $login = User::getLogin();
            $banner_title = xss($_POST["Banner"]["title"]);
            $banner_text = xss($_POST["Banner"]["text"]);
            $banner_img_url = xss($_POST["Banner"]["img_url"]);
            $banner_period_individual = xss($_POST["Banner"]["period_individual"]);
            $model->author = User::getLogin(); // получаем логин для внесения в БД
            $model->date = time();
            $model->datetime = date('Y-m-d H:i:s');
            $model->period_individual = $banner_period_individual;
            $imageName = Tools::getRandomName();
            $model->author = Yii::$app->user->identity->login; // получаем логин для внесения в БД
            $imageName = Yii::$app->security->generateRandomString();
            $model->logo = UploadedFile::getInstance($model, 'logo'); // получаем объект файла (баннер)
            if (empty(xss($_POST["Banner"]["text"])) && empty(xss($_POST["Banner"]["img_url"])) ) {
                $model->img = 'uploads/' . $imageName . '.' . $model->logo->extension; // сохраняем адрес объекта (баннера) в
            }
            if ( !empty(xss($_POST["Banner"]["text"])) ) {
                $imageName = md5(uniqid()).sha1(uniqid());
                $model->img = 'uploads/' . $imageName; // сохраняем адрес объекта (баннера) в
            }
            if ( !empty (xss($_POST["Banner"]["img_url"]))) {
                $model->img_url = xss($_POST["Banner"]["img_url"]);
            }
            $model->url = xss($_POST["Banner"]["url"]); // ЭКВИВАЛЕНТ 'value' = 'http://www.' ЧТОБЫ В ПОЛЕ УЖЕ БЫЛО ВВЕДЕНО НАЧАЛО
//            $model->cost = 0.08;
//            $model->time = time();
//            $model->status = 1;
//            $model->datetime = date('Y-m-d H:m:s');
            $model->save();
            if (empty(xss($_POST["Banner"]["text"])) && empty(xss($_POST["Banner"]["title"])) && empty(xss($_POST["Banner"]["img_url"])) ) {
                $model->logo->saveAs('uploads/' . $imageName . '.' . $model->logo->extension); // физически переносим объект (баннер) в папку
            }

//            $now = time();
//            $datetime = date('Y-m-d H:i:s');
//            $message = "Пользователь $login <b>добавил новый баннер</b>";
//            Yii::$app->db->createCommand("INSERT INTO `event` (`message`, `date`, `datetime`) VALUES (:message, :date, :datetime)")
//                ->bindParam(':message', $message)
//                ->bindParam(':date', $now)
//                ->bindParam(':datetime', $datetime)
//                ->execute();

//            Yii::$app->session->setFlash('uploaded_ok', "Ваш баннер успешно загружен! Посмотреть <a href='".Url::to(['banner-my'])."' style='text-decoration: none; border-bottom:1px dotted'>мои баннеры</a>");
            Session::setFlash('success', 'Ваш баннер успешно загружен!');
        }



        return $this->render('banners', compact('model', 'success')); // всю эту информацию перекидываем на страницу upload.php
    }

    public function actionBannerShow()
    {
//        $banners = Banner::getActiveBannerByAuthor(User::getLogin());
        $login = User::getLogin();
        $time_passed = ClickBanner::timePassed($login);
        $time_left = BANNER_CLICK_PERIOD - $time_passed;
        $banners = Banner::getByConditionRand(1, null, 10);
        return $this->render('banner-show', compact('login', 'banners', 'time_passed', 'time_left'));
    }

    public function actionBannersMy()
    {
        $banner = new Banner();
        $banners = Banner::getByAuthor(User::getLogin());
        return $this->render('banners-my', compact('banners', 'banner'));
    }

    public function actionBannerMy(){

        $query = Banner::find()
            ->where(['=', 'author', User::getLogin()])
            ->andWhere(['<>', 'status', 'delete']) // все выкл/вкл, удалённые не показываем
            ->orderBy(['date' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['defaultPageSize' => 5, 'totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('banner-my', [
            'models' => $models,
            'pages' => $pages,
        ]);


//        return $this->render('banner-my', compact('banner'));
    }

    public function actionBannerPause()
    {
        Session::set('bannerPause', true);
        Banner::pause(Request::post('id'));
        echo "1";
    }

    public function actionBannerPlay()
    {
        Session::set('bannerPlay', true);
        Banner::play(Request::post('id'));
        echo "1";
    }

    public function actionBannerRemove()
    {
        $id = Request::post('id');
        Banner::remove($id);
        echo $id;
    }

    public function actionBannerChangeUrl()
    {
        $id = Request::post('id');
        $url = Request::post('url');
        Yii::$app->db->createCommand("UPDATE `banner` SET `url` = :url WHERE `id` = :id")
            ->bindParam(':url', $url)
            ->bindParam(':id', $id)
            ->execute();
        echo $id;
    }

    public function actionBannerChangeTitle()
    {
        $id = Request::post('id');
        $title = Request::post('title');
        Yii::$app->db->createCommand("UPDATE `banner` SET `title` = :title WHERE `id` = :id")
            ->bindParam(':title', $title)
            ->bindParam(':id', $id)
            ->execute();
        echo $id;
    }

    public function actionBannerChangeText()
    {
        $id = Request::post('id');
        $text = Request::post('text');
        Yii::$app->db->createCommand("UPDATE `banner` SET `text` = :text WHERE `id` = :id")
            ->bindParam(':text', $text)
            ->bindParam(':id', $id)
            ->execute();
        echo $id;
    }

    public function actionBannerChangeImgurl()
    {
        $id = Request::post('id');
        $img_url = Request::post('new_img_url');
        Yii::$app->db->createCommand("UPDATE `banner` SET `img_url` = :img_url WHERE `id` = :id")
            ->bindParam(':img_url', $img_url)
            ->bindParam(':id', $id)
            ->execute();
        echo $id;
    }
	
	public function actionBannerChangeCost()
    {
        $banner_id = Request::get('banner_id');
        $banner_cost = Request::get('banner_cost');
        Yii::$app->db->createCommand("UPDATE `banner` SET `cost` = :cost WHERE `id` = :id")
            ->bindParam(':cost', $banner_cost)
            ->bindParam(':id', $banner_id)
            ->execute();
        echo $banner_id;
    }

    public function actionBannerEarn(){

        if ( $_SERVER["REQUEST_METHOD"] != "POST" ) exit('Нужно нажать по баннеру');

        if ( User::getPeriodFromLastClick(User::getLogin()) < BANNER_CLICK_PERIOD ) exit(MESSAGE_TIME_NOT_PASSED_YET_BANNER);

        if ( ClickBanner::timePassed(User::getLogin()) > BANNER_CLICK_PERIOD )
        {
            $banner_id = Request::post('banner-id');
            $banner_cost = Request::post('banner-cost');
            $banner_author = Request::post('banner-author');
            $banner_url = Request::post('banner-url');
            $banner_img = Request::post('banner-img');
            $banner_title = Request::post('banner-title');
            $banner_text = Request::post('banner-text');
            $banner_watch_time = Request::post('banner-watch-time');

            //User::payForBannerClick($banner_cost * USER_COMISSION);
            //User::changeAdvBalance($banner_author, -$banner_cost); //списываем полную стоимость (комиссия не учитывается, нужно содрать полностью)
            //ClickBanner::add();
            //Banner::visit($banner_id);

            return $this->render('banner-earn', compact('banner_id', 'banner_cost', 'banner_author', 'banner_url', 'banner_img', 'banner_title', 'banner_text', 'banner_watch_time'));
        }
        else
        {
            echo 'time_not_passed_yet';
//            exit();
        }



    }

    // action кнопки начисления денег.
    // BannerEarn - просто страница просмора и вывода кнопки
    // BannerPay - action действия оплаты и снятия
    public function actionBannerPay(){

        if ( ClickBanner::timePassed(User::getLogin()) > BANNER_CLICK_PERIOD ) {

            $banner_id = Request::get('banner_id');
            $banner_cost = Request::get('banner_cost');
            $banner_author = Request::get('banner_author');
            $banner_url = Request::get('banner_url');
            $banner_img = Request::get('banner_img');
            $banner_title = Request::get('banner_title');
            $banner_text = Request::get('banner_text');
            $banner_watch_time = Request::get('banner_watch_time');

            $login = User::getLogin();

            //$banner_cost = Request::get('banner_cost');

            try {
                //если есть аватарка
                if(User::hasAvatar($login)){
                    User::payForBannerClick($banner_cost * USER_COMISSION * PAY_FOR_AVATAR * ADMIN_PRIZE_PERCENT);
                } else {
                    //если нет аватарки
                    User::payForBannerClick($banner_cost * USER_COMISSION * ADMIN_PRIZE_PERCENT);
                }
            } catch (\Exception $e) {
                echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            }


            try {
                if (User::getRefovodId()) {
                    User::payHisRefovodBonus($banner_cost * USER_COMISSION); // на данном сайте это просто учёт бонуса в БД

                }
            } catch (\Exception $e) {
                echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            }
           // User::changeAdvBalance($banner_author, -$banner_cost); //списываем полную стоимость (комиссия не
            // учитывается, нужно содрать полностью)

            $banner_cost_with_user_comission = $banner_cost * USER_COMISSION * ADMIN_PRIZE_PERCENT;

           /* Yii::$app->db->createCommand("
                UPDATE `user`
                SET `adv_balance` = `adv_balance` - :banner_cost
                WHERE `login` = :banner_author
            ")
                ->bindParam(':banner_cost', $banner_cost)
                ->bindParam(':banner_author', $banner_author)
                ->execute();*/

           User::author_MinusCost_For_Click($banner_author, $banner_cost);

            //User::payHisRefovod($banner_cost);
            ClickBanner::add();
            Banner::visit(Request::get('banner_id'));
            $banner_cost_to_pay_for_click = $banner_cost * USER_COMISSION * ADMIN_PRIZE_PERCENT;
            $message = "<img src=\"/web/img/pointer.png\" width=\"20\" height=\"20\"/> &nbsp; Пользователь <strong>$login</strong> заработал на клике по баннеру <span style='color:blue; font-weight: bold;'> $banner_cost_to_pay_for_click </span> руб";
            Event::add($login, $message);

            echo 'success';
        }
        else
        {
            echo 'time_not_passed_yet';
//            exit();
        }


    }

    public function actionLoginCheck()
    {
        $login = Request::get('login');

        if (User::checkLogin($login))
        {
            echo 'exists';
        }
        else
        {
            echo 'no_exists';
        }

    }

	public function actionUserIPCheck()
	{

		if (User::getIP())
		{
			echo 'exists';
		}
		else
		{
			echo 'no_exists';
		}

	}
	
	public function actionChangePoints(){
		$userPoints = User::getPoints();
		$points = Request::get('points');
		
		if ($points > $userPoints)
		{
			exit("MorePointsThanYouHave");
		}
		else
        {
            User::changePointsToCash($points);
            echo 'points_to_cash_success';
        }
	}

	// майнинг добавление в базу данных
	public function actionAjaxMining(){
		$bonus_sum = Request::get('bonus_sum') * USER_COMISSION * ADMIN_PRIZE_PERCENT;
		$login = User::getLogin();
		User::changeBalance($login, $bonus_sum);

        //оплата рефоводу
        try {
            if ($login != 'Гость'){
                $avatar = User::getAvatarOnID(User::getUserId());
                $message = "<img src=\"$avatar\" width=\"30\" height=\"30\"/> &nbsp; Пользователь <strong>$login</strong> 
намайнил <span style='color:blue; font-weight: bold;'>" . number_format($bonus_sum,5) . "</span> руб";


                /*$message = "<img src=\"/web/img/miner.png\" width=\"20\" height=\"20\"/> &nbsp; Пользователь <strong>$login</strong>
намайнил <span style='color:blue; font-weight: bold;'>" . number_format($bonus_sum,5) . "</span> руб";*/

                Event::add($login, $message);
            }

        } catch (\Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }

        try {
            if (User::getRefovodId()) {
                User::payHisRefovodBonus($bonus_sum); // на данном сайте это просто учёт бонуса в БД

            }
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }

        try {
            Bonus::add($bonus_sum);
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }

		echo 'success';
	}

	public function actionSiteRegPay(){
		$site = Request::get('site');
		$login = Request::get('login');
		if (!Sitereg::isSend()){
			Sitereg::add();
		};

		//return $this->render('site-reg-pay', compact('site'));
	}

	public function actionPollYes(){
	    $id_user = User::getUserId();
        $time = time();

        Yii::$app->db->createCommand()
            ->insert('pollRefCompetition', [
                'id_user' => $id_user,
                'answer' => 1,
                'time' => $time
            ])->execute();


    }

    public function actionRefs(){
        News::addVisit(14);
	    $news_array = News::find()->where(['category' => 'refs'])->all();
	    return $this->render('refs', compact('news_array'));
    }

    public function actionMessages(){
        $id = User::getUserId();
        $from = Request::get('from');

       // if (!Messages::ifSent($from)) exit('You can\'t see messages of other users');

        $messages_data = Messages::find()
            ->where(['from' => $id])
            ->orWhere(['to' => $id])
            ->orWhere(['from' => $from])
            ->orWhere(['to' => $from])
            ->orderBy('time asc')
            ->all();

        $my_id = User::getUserId();

    Yii::$app->db->createCommand("
        UPDATE `messages`
        SET `seen` = 1
        WHERE `to` = $my_id
    ")->execute();


        return $this->render('messages', compact('messages_data', 'id', 'from'));
    }

    public function actionMsg(){
        $id = User::getUserId();
        $from = Request::get('from');
       /* $messages_data = Messages::find()
            ->where(['to' => $id])
            ->orWhere(['from' => $id])
            ->orWhere(['from' => $from])
            ->orWhere(['to' => $from])
            ->orderBy('id asc')
            ->groupBy('from')
            ->all();*/

        $messages_data = Yii::$app->db->createCommand("
                SELECT * 
                FROM `messages`
                WHERE `to` = $id
                OR `from` = $id
        ")->queryAll();

        return $this->render('msg', compact('messages_data', 'id'));
    }

    public function actionMessageSend(){
	    $msg = Request::get('msg');
	    $to = Request::get('to');
	    $from = Request::get('from');
	    $time = time();
	    $datetime = date('Y-m-d H:i:s');
	    $ip = Request::get('ip');

	    Yii::$app->db->createCommand("
	        INSERT INTO `messages` (`from`, `to`, `content`, `time`, `datetime`, `ip`)
	        VALUES(:from, :to, :content, :time, :datetime, :ip)
	    ")
            ->bindParam(':to', $to)
            ->bindParam(':from', $from)
            ->bindParam(':content', $msg)
            ->bindParam(':time', $time)
            ->bindParam(':datetime', $datetime)
            ->bindParam(':ip', $ip)
            ->execute();
    }

    public function actionGames(){
	    $games_data = Games::find()->all();
	    return $this->render('games', compact('games_data'));
    }

    public function actionGameDownloadCount(){
	    $id = Request::get('id');
	    Yii::$app->db->createCommand("
	        UPDATE `games`
	        SET `downloads` = `downloads` + 1
	        WHERE `id` = :id
	    ")->bindParam(':id', $id)->execute();

	    echo 'success';
    }

    public function actionAjaxAddNickname(){
        $nickname = Request::get('nickname');
        $exist = User::isNicknameExists($nickname);
        if (null != $exist) {
            echo 'exist';
        } else {
            User::addNickname($nickname);
            User::addGreetings();
            echo 'success';
        }
    }

    public function actionAjaxAddGreetings(){
        $by_name = Request::get('byName');

        if ($by_name == 'nickname') {
            Yii::$app->db->createCommand("
                UPDATE `user`
                SET `greetings` = :greetings
                WHERE `id` = :id
            ")
                ->bindParam(':greetings', $by_name)
                ->bindParam(':id', $id)
                ->execute();
        }

    }

    public function actionAjaxChangeAvatar(){
        $id = User::getUserId();
        $avatar_url = Request::get('avatar_url');
        $avatar = User::getAvatar();
        if($avatar){
            Yii::$app->db->createCommand("
            UPDATE `user`
            SET `avatar_url` = :avatar_url
            WHERE `id` = :id 
        ")
                ->bindParam(':avatar_url', $avatar_url)
                ->bindParam(':id', $id)
                ->execute();
        } else {
            Yii::$app->db->createCommand()
                ->update('user', [
                    'avatar_url' => $avatar_url,
                ], "id=$id")
                ->execute();
        }

        echo 'success';
    }

    public function actionAjaxSupportAds(){
        $id = User::getUserId();
        $ip = User::getIP();
        $time = time();
        $datetime = date('Y-m-d H:i:s');
        $message = Request::get('message');
        Yii::$app->db->createCommand()
            ->insert('supportAds', [
                'id_user' => $id,
                'text' => $message,
                'ip' => $ip,
                'time' => $time,
                'datetime' => $datetime,
            ])
            ->execute();
        echo 'success';
        }

    public function actionNews(){
        $id = (int)Request::get('id');
        $news = News::find()->where(['id' => $id])->all();
        News::addVisit($id);
        $news_comments = NewsComment::find()->where(['id_news' => $id])->all();
        return $this->render('news', compact('news', 'news_comments'));
    }

    public function actionAdminGetAllDataByLogin(){
        $login = Request::get('login');
        $id_ref_user = User::find()->where(['login' => $login])->all();
        //$id = $id_ref_user[0]['id'];

      //  $users_array = User::find()->where(['login' => $login])->all();
      //  $users_refs_count = User::find()->where(['id_ref' => 1])->count();

        /*echo 'id: '.$users_array[0]['id'] .'<br>'
            .'Кошелек: '.$users_array[0]['id'] .'<br>'
            .'Ник: '.$users_array[0]['nickname'] .'<br>'
            .'Аватарка: '.$users_array[0]['avatar_url'] .'<br>'
            .'IP: '.$users_array[0]['ip'] .'<br>'
            .'Заработано на реферальных: '.$users_array[0]['points_ref_1'] .'<br>'
            .'На счету: '.$users_array[0]['balance'] .'<br>'
            .'Рекл баланс: '.$users_array[0]['adv_balance'] .'<br>'
            .'Дата регистрации: '.$users_array[0]['datetime'] .'<br>'
        ;*/

        echo $login;
    }

    public function actionAutojobAdd(){

        $model = new Autojob();

        if ($model->load(Yii::$app->request->post()))
        {
            $model->id_author = User::getUserId();
            $model->title = xss($_POST["Autojob"]["title"]);
            $model->content = xss($_POST["Autojob"]["content"]);
            $model->url = xss($_POST["Autojob"]["url"]);
            $model->keyword = xss($_POST["Autojob"]["keyword"]);
            //наша прибыль учитывается тут
            $model->cost = xss($_POST["Autojob"]["cost"]);
            $model->ip = User::getIP();
            $model->time = time();
            $model->datetime = date('Y-m-d H:i:s');
            $model->save();

        }

        return $this->render('autojob-add', compact('model'));
    }

    public function actionAutojobs(){

        $user = User::find()->where(['id' => User::getUserId()])->one();

        $autojobs = Autojob::find()
            ->joinWith('user')
            ->where(['<>', 'status', 'delete'])
            ->andWhere(['>', 'adv_balance', 0.12]) //тут было просто where() таким образом выводило лишнее
            ->orderBy('cost desc')
            ->all();



        return $this->render('autojobs', compact('user', 'autojobs'));
    }

    public function actionAutojobFull(){

        $id_job = Request::get('id');
        $autojob_full = Autojob::find()->where(['id' => $id_job])->one();
        $user = User::find()->where(['id' => User::getUserId()])->one();
        $autojob_array = \app\models\Autojob::find()->all();
        $comments = AutojobComments::find()->where(['id_autojob' => 1])->all();
        return $this->render('autojob-full', compact('user', 'autojob_full', 'autojob_array', 'comments1'));
    }

    //деньги снимаются тут и наша часть учитывается тут
    public function actionAjaxAutojobCheck(){
        $answer = Request::get('answer');
        $id_job = Request::get('id_job');
        $id_user = Request::get('id_user');
        $cost = Request::get('cost');
        $id_author = Request::get('id_author');

        $autojob_array = Autojob::find()->where(['id' => $id_job])->all();
        $keyword = $autojob_array[0]->keyword;

        if(AutojobDone::isDoneByJobID($id_job, $id_user)){
            exit('done');
        }

        if(AutojobDone::isFailByJobID($id_job, $id_user)){
            exit('fail');
        }

        //если ввёл правильное слово
        if (strtolower($answer) == strtolower($keyword)){
            AutojobDone::makeDone($id_job, $id_user);
            //снимаются с рекламодателя деньги + 20% нам
            User::changeAdvBalanceByID($id_author, -($cost + $cost * OUR_COMISSION ) );
            // юзеру платится полная сумма стоимости задания
            User::changeBalanceByID($id_user, $cost);
            echo 'success';
        }
        //если ввёл неправильное слово
        if (strtolower($answer) != strtolower($keyword)){
            //если в AutojobDone ешё записей вообще нет по данным $id_autojob и $id_user, то вставляем 1-ю запись
            if(!AutojobDone::existsByJobAndUser($id_job, $id_user)){
                $id_user = Request::get('id_user');
                $id_autojob = Request::get('id_job');
                $cost = Request::get('cost');
                $attempt = 1;
                $status = 'try_again';
                $ip = User::getIP();
                $time = time();
                $datetime = date('Y-m-d H:i:s');
                Yii::$app->db->createCommand("
                INSERT INTO `autojobDone` (`id_user`, `id_autojob`, `cost`, `attempt`, `status`, `ip`, `time`, `datetime`)
                VALUES(:id_user, :id_autojob, :cost, :attempt, :status, :ip, :time, :datetime)
                ")
                    ->bindParam(':id_user', $id_user)
                    ->bindParam(':id_autojob', $id_autojob)
                    ->bindParam(':cost', $cost)
                    ->bindParam(':attempt', $attempt)
                    ->bindParam(':status', $status)
                    ->bindParam(':ip', $ip)
                    ->bindParam(':time', $time)
                    ->bindParam(':datetime', $datetime)
                    ->execute();
                echo 'try_again_insert';
            }
            else
            {

                /*if (AutojobDone::getCountAttempts($id_job, $id_user) >= 2) {
                    Yii::$app->db->createCommand("

                    ")
                        ->bindParam(':id_user', $id_user)
                        ->bindParam(':id_autojob', $id_autojob)
                        ->execute();
                    echo 'attempts_exceeded';
                }*/

                if (AutojobDone::getCountAttempts($id_job, $id_user) < 2 ){
                    $count = AutojobDone::getCountAttempts($id_job, $id_user);
                    // если кол-во попыток меньше 5 - продолжаем попытки и увеличиваем счётчик
                    $id_user = Request::get('id_user');
                    $id_autojob = Request::get('id_job');
                    $cost = Request::get('cost');
                    $attempt = 1;
                    $status = 'try_again';
                    $ip = User::getIP();
                    $time = time();
                    $datetime = date('Y-m-d H:i:s');
                    Yii::$app->db->createCommand("
                    UPDATE `autojobDone`
                    SET `attempt` = `attempt` + 1
                    WHERE `id_user` = :id_user
                    AND `id_autojob` = :id_autojob
                ")
                        ->bindParam(':id_user', $id_user)
                        ->bindParam(':id_autojob', $id_autojob)
                        ->execute();
                    echo 'try_again_update';
                }

                else if (AutojobDone::getCountAttempts($id_job, $id_user) == 2 ){
                    $count = AutojobDone::getCountAttempts($id_job, $id_user);
                    // если кол-во попыток меньше 5 - продолжаем попытки и увеличиваем счётчик
                    $id_user = Request::get('id_user');
                    $id_autojob = Request::get('id_job');
                    $cost = Request::get('cost');
                    $attempt = 1;
                    $status = 'try_again';
                    $ip = User::getIP();
                    $time = time();
                    $datetime = date('Y-m-d H:i:s');
                    Yii::$app->db->createCommand("
                    UPDATE `autojobDone`
                    SET `status` = 'fail'
                    WHERE `id_user` = :id_user
                    AND `id_autojob` = :id_autojob
                ")
                        ->bindParam(':id_user', $id_user)
                        ->bindParam(':id_autojob', $id_autojob)
                        ->execute();
                    echo 'attempts_exceeded';
                }

                /*else if (AutojobDone::getCountAttempts($id_job, $id_user) >= 3){
                    $count = AutojobDone::getCountAttempts($id_job, $id_user);
                        Yii::$app->db->createCommand("

                    ")
                            ->bindParam(':id_user', $id_user)
                            ->bindParam(':id_autojob', $id_autojob)
                            ->execute();
                    echo 'attempts_exceeded';
                }*/
                else {
                    echo 'Unknown error';
                }


            }
        }


    }

    public function actionAjaxAutojobDone(){
        $id_job = Request::get('id_job');
        $id_user = Request::get('id_user');
        $ip = User::getIP();
        $time = time();
        $datetime = date('Y-m-d H:i:s');

       AutojobDone::makeDone($id_job, $id_user);

       echo 'success';


    }

    public function actionAutojobMy(){
        $user = User::find()->where(['id' => User::getUserId()])->one();
        $id_user = User::getUserId();
        $autojobs = Autojob::getByAuthorID($id_user);

        return $this->render('autojob-my', compact('user', 'autojobs'));
    }

    public function actionAutojobPause()
    {
        Session::set('autojobPause', true);
        Autojob::pause(Request::post('id'));
        echo "1";
    }

    public function actionAutojobPlay()
    {
        Session::set('autojobPlay', true);
        Autojob::play(Request::post('id'));
        echo "1";
    }

    public function actionAutojobRemove()
    {
        $id = Request::post('id');
        Autojob::remove($id);
        echo $id;
    }

    public function actionUserOnline(){
        $id_user = Request::get('id_user');
        $status = 'online';
        $time = time();
        $exist = UserOnline::find()->where(['id_user' => $id_user])->exists();
        if (!$exist) {
            Yii::$app->db->createCommand("
                INSERT INTO `userOnline` (`id_user`, `status`, `time`)
                VALUES(:id_user, :status, :time)
            ")
                ->bindParam(':id_user', $id_user)
                ->bindParam(':status', $status)
                ->bindParam(':time', $time)
                ->execute();
        } else {
            Yii::$app->db->createCommand("
                UPDATE `userOnline`
                SET `id_user` = :id_user, 
                    `status` = :status, 
                    `time` = :time
                WHERE `id_user` = :id_user
            ")
                ->bindParam(':id_user', $id_user)
                ->bindParam(':status', $status)
                ->bindParam(':time', $time)
                ->execute();
        }
    }

    public function actionUserOffline(){
        $id_user = Request::get('id_user');
        $status = 'offline';
        $time = time();
        $exist = UserOnline::find()->where(['id_user' => $id_user])->exists();
            Yii::$app->db->createCommand("
                UPDATE `userOnline`
                SET `id_user` = :id_user, 
                    `status` = :status, 
                    `time` = :time
                WHERE `id_user` = :id_user
            ")
                ->bindParam(':id_user', $id_user)
                ->bindParam(':status', $status)
                ->bindParam(':time', $time)
                ->execute();
    }

    public function actionAjaxAddCommentAutojob() {
        $comment = Request::get('comment');
        $id_autojob = Request::get('id_autojob');
        $id_user = Request::get('id_user');
        $status = 'published';
        $timestamp = time();
        $datetime = date('Y-m-d H:i:s');
        $ip = User::getIP();


        Yii::$app->db->createCommand("
            INSERT INTO `autojobComments` (`id_user`, `id_autojob`,`status`, `text`, `timestamp`, `datetime`, `ip`)
            VALUES(:id_user, :id_autojob, :status, :text, :timestamp, :datetime, :ip)
        ")
            ->bindParam(':id_user', $id_user)
            ->bindParam(':id_autojob', $id_autojob)
            ->bindParam(':status', $status)
            ->bindParam(':text', $comment)
            ->bindParam(':timestamp', $timestamp)
            ->bindParam(':datetime', $datetime)
            ->bindParam(':ip', $ip)
            ->execute();

        echo 'success';
    }

    public function actionAjaxAddCommentNews() {
        $comment = Request::get('comment');
        $id_news = Request::get('id_news');
        $id_user = Request::get('id_user');
        $timestamp = time();
        $datetime = date('Y-m-d H:i:s');
        $ip = User::getIP();


        Yii::$app->db->createCommand("
            INSERT INTO `newsComment` (`id_news`, `id_user`, `text`, `timestamp`, `datetime`, `ip`)
            VALUES(:id_news, :id_user, :text, :timestamp, :datetime, :ip)
        ")
            ->bindParam(':id_news', $id_news)
            ->bindParam(':id_user', $id_user)
            ->bindParam(':text', $comment)
            ->bindParam(':timestamp', $timestamp)
            ->bindParam(':datetime', $datetime)
            ->bindParam(':ip', $ip)
            ->execute();

        echo 'success';
    }

    public function actionCompetitionCollection(){

        return $this->render('competition-colelction');
    }

    public static function userInRatingExists($id_user){
        $exist = Rating::find()->where(['id_user' => $id_user])->exists();
        return $exist;
    }

    public function actionAddBannerClickCount() {
        $id_user = Request::get('id_user');
        if (self::userInRatingExists($id_user)){
            Yii::$app->db->createCommand("
            UPDATE `rating`
            SET `banners_clicks` = `banners_clicks` + 1
            WHERE `id_user` = :id_user
        ")
                ->bindParam(':id_user', $id_user)
                ->execute();
        } else {
            Yii::$app->db->createCommand("
                INSERT INTO `rating`(`id_user`, `banners_clicks`)
                VALUES (:id_user, 1)
            ")
                ->bindParam(':id_user', $id_user)
                ->execute();
        }



    }

    public function actionReviewsAndRatings(){
        $site_array = Sitelist::find()->all();
        return $this->render('reviews-and-ratings', compact('site_array'));
    }

    public function actionReviewsAndRatingsFull(){
        $id = (int)Request::get('id');
        $site_array = Sitelist::find()->where(['id' => $id])->all();
        $sitelist_array = SitelistComments::getAll($id);
        $myScore = ceil(SitelistRating::getMyScore(User::getUserId(), (int)$_GET['id']));
        $average = SitelistRating::ofThisSite($id);

        return $this->render('reviews-and-ratings-full', compact('site_array', 'sitelist_array', 'myScore', 'average'));

    }

    public function actionAddSitelistComments(){
        $id_user = Request::post('id_user');
        $id_site = Request::post('id_site');
        $type = Request::post('type');
        $forpay = Request::post('forPay');
        $comment = Request::post('comment');
        $time = time();
        Yii::$app->db->createCommand("
            INSERT INTO `sitelistComments`(`id_user`, `id_site`, `comment`, `commentType`, `forpay`, `time`)
            VALUES(:id_user, :id_site, :comment, :type, :forpay, :time)
        ")
            ->bindParam(':id_user', $id_user)
            ->bindParam(':id_site', $id_site)
            ->bindParam(':comment', $comment)
            ->bindParam(':type', $type)
            ->bindParam(':forpay', $forpay)
            ->bindParam(':time', $time)
            ->execute();
        echo 'success';

    }

    public function actionAddRating($score){
        SitelistRating::addNew($score);
        echo 'success';


    }

    public function actionPayForSiteReview(){
        $id_user_to_pay = Request::get('id_user_to_pay');
        $id_site = Request::get('id_site');
        $id = Request::get('id');

        $pay_for_site_review = PAY_FOR_SITE_REVIEW;
        // пополним баланс пользователя
        Yii::$app->db->createCommand("
            UPDATE `user`
            SET `balance` = `balance` + :pay_for_site_review
            WHERE `id` = :id_user
        ")
            ->bindParam(':pay_for_site_review', $pay_for_site_review)
            ->bindParam(':id_user', $id_user_to_pay)
            ->execute();

        // отметим paid=1(оплачено)
        Yii::$app->db->createCommand("
            UPDATE `sitelistComments`
            SET `paid` = 1
            WHERE `id_user` = :id_user
            AND `id_site` = :id_site
            AND `id` = :id
        ")
            ->bindParam(':id_user', $id_user_to_pay)
            ->bindParam(':id_site', $id_site)
            ->bindParam(':id', $id)
            ->execute();

        echo 'success';
    }

}
