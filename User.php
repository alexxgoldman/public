<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\IdentityInterface;
use Yii;
use app\classes\Request;

class User extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return USER_DB_TABLE;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->authKey = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public function getAutojob(){
        return $this->hasMany(Autojob::className(), ['id_author' => 'id']);
    }

    public function getBanner(){
        return $this->hasMany(Banner::className(), ['author' => 'login']);
    }

    public function getSitelistComments(){
        return $this->hasMany(SitelistComments::className(), ['id_user' => 'id']);
    }


    // возвращает логин пользователя
    public static function getLogin()
    {
    	return !empty(Yii::$app->user->identity->login) ? Yii::$app->user->identity->login : 'Гость';
    }

    public static function getAll(){
        $all = User::find()->all();
        return $all;
    }

    // шифруем пароль
    public static function encodePassword($password)
    {
        $password = sha1($password.SALT_1)
                    .md5($password.SALT_2)
                    .sha1(md5($password.SALT_3))
                    .md5(SALT_1.SALT_2.SALT_3);
        return $password;
    }

    // устанавливает зашифрованный пароль пользователя при регистрации
    public function setPassword($password)
    {
        $this->password = self::encodePassword($password);
    }

    // проверяем зашифрованные пароли при авторизации
    public function validatePassword($password)
    {
        return $this->password === self::encodePassword($password);
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public static function findByUsername($username)
    {
        return self::findOne($username);
    }

    // возвращает id пользователя
    public function getId()
    {
        return $this->id;
    }

    public static function getUserId()
    {
       return Yii::$app->user->getId();
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

//    public function validatePassword($password)
//    {
//        return $this->password === $password;
//    }

    public static function isLogin()
    {
        return !Yii::$app->user->isGuest;
    }

    public static function isGuest()
    {
        return Yii::$app->user->isGuest;
    }
	
	// возвращает баллы пользователя
	 public static function getPoints()
    {
        $user = User::find()->where(['login' => User::getLogin()])->all();
        $points = round($user[0]['points'],2);
        return $points;
    }

    public static function getNickname()
    {
        $user = User::find()->where(['login' => User::getLogin()])->all();
        $nickname = $user[0]['nickname'];
        return $nickname;
    }

    public static function isNicknameExists($nickname)
    {
        $nickname = xss($nickname);
        $exists = User::find()->where(['nickname' => $nickname])->exists();
        return $exists;
    }

    public static function addNickname($nickname)
    {
        $id = (int)User::getUserId();
        $nickname = xss($nickname);
        Yii::$app->db->createCommand("
                UPDATE `user` 
                SET `nickname` = :nickname
                WHERE `id` = :id
        ")
            ->bindParam(':nickname', $nickname)
            ->bindParam(':id', $id)
            ->execute();
    }

    public static function getGreetings()
    {
        $user = User::find()->where(['login' => User::getLogin()])->all();
        $greetings = $user[0]['greetings'];
        return $greetings;

    }

    public static function getAvatar()
    {
        $user = User::find()->where(['login' => User::getLogin()])->all();
        $avatar = $user[0]['avatar_url'];
        if (null == $avatar) {
            $a = 'https://goo.gl/G1CHYo';
        } else {
            $a = $user[0]['avatar_url'];
        }
        return $a;

    }

    public static function hasAvatar($login)
    {
        $default_avatar = 'https://goo.gl/G1CHYo';
        $user = User::find()
            ->where(['login' => $login])
            ->one();
        $avatar = $user->avatar_url;
        if ($avatar == $default_avatar){
            return 0;
        } elseif ($avatar == null) {
            return 0;
        } else {
            return 1;
        }


    }

    public static function hasAvatarOnID($id)
    {
        $default_avatar = 'https://goo.gl/G1CHYo';
        $user = User::find()
            ->where(['id' => $id])
            ->one();
        $avatar = $user->avatar_url;
        if ($avatar == $default_avatar){
            return 0;
        } elseif ($avatar == null) {
            return 0;
        } else {
            return 1;
        }


    }


    public static function getAvatarOnID($id)
    {
        $user = User::find()->where(['id' => $id])->all();
        $avatar = $user[0]['avatar_url'];
        if (null == $avatar) {
            $a = 'https://goo.gl/G1CHYo';
        } else {
            $a = $user[0]['avatar_url'];
        }
        return $a;

    }

    public static function addGreetings()
    {
        $id = (int)User::getUserId();
        $nickname = 'nickname';
        Yii::$app->db->createCommand("
                UPDATE `user` 
                SET `greetings` = :greetings
                WHERE `id` = :id
        ")
            ->bindParam(':greetings', $nickname)
            ->bindParam(':id', $id)
            ->execute();
    }

	public static function changePointsToCash($points)
	{
		$login = User::getLogin();
		Yii::$app->db->createCommand("UPDATE `user` SET `points` = `points` - :value WHERE `login` = :login")
            ->bindParam('login', $login)
            ->bindParam('value', $points)
            ->execute();
		
		$pointsToCash = $points * POINTS_CHANGE_RATIO;
		Yii::$app->db->createCommand("UPDATE `user` SET `balance` = `balance` + :value WHERE `login` = :login")
            ->bindParam('login', $login)
            ->bindParam('value', $pointsToCash)
            ->execute();
	}

    // возвращает баланс пользователя
    public static function getBalance()
    {
        $user = User::find()->where(['login' => User::getLogin()])->all();
        $balance = round($user[0]['balance'],8);
        return $balance;

    }

    // возвращает рекламный баланс пользователя
    public static function getAdvBalance()
    {
        $user = User::find()->where(['login' => User::getLogin()])->all();
        $adv_balance = round($user[0]['adv_balance'],2);
        return $adv_balance;

    }

    // возвращает рекламный баланс пользователя $login
    public static function getAdvBalanceByLogin($login)
    {
        $user = User::find()->where(['login' => $login])->all();
        $adv_balance = round($user[0]['adv_balance'],2);
        return $adv_balance;

    }

    // возвращает ip пользователя
    public static function getIP()
    {
        return $_SERVER["REMOTE_ADDR"];
    }

    // возращает браузер пользователя
    public static function getBroswer()
    {
        return Yii::$app->request->getUserAgent();
    }

    public static function getLastClick($login)
    {
        $user = ClickBanner::find()->where(['login' => $login])->max('date');
        return $user;
    }

    public static function getPeriodFromLastClick($login)
    {
        return time()-self::getLastClick($login);
    }


    // ВНИМАНИЕ ЗНАК! +$value, -$value. изменить баланс пользователя $login на заданную величину $value
    public static function changeBalance($login, $value)
    {
        $user_db_table = USER_DB_TABLE;
        Yii::$app->db->createCommand("UPDATE $user_db_table SET `balance` = `balance` + :value WHERE `login` = :login")
            ->bindParam(':login', $login)
            ->bindParam(':value', $value)
            ->execute();
    }

    public static function changeBalanceByID($id, $value)
    {
        $user_db_table = USER_DB_TABLE;
        Yii::$app->db->createCommand("UPDATE $user_db_table SET `balance` = `balance` + :value WHERE `id` = :id")
            ->bindParam(':id', $id)
            ->bindParam(':value', $value)
            ->execute();
    }

    // изменить баланс пользователя $login на заданную величину $value
    public static function changeAdvBalance($login, $value)
    {
        $user_db_table = USER_DB_TABLE;
        Yii::$app->db->createCommand("UPDATE $user_db_table SET `adv_balance` = `adv_balance` + :value WHERE `login` = :login")
            ->bindParam(':login', $login)
            ->bindParam(':value', $value)
            ->execute();
    }

    public static function changeAdvBalanceByID($id, $value)
    {
        $user_db_table = USER_DB_TABLE;
        Yii::$app->db->createCommand("UPDATE $user_db_table SET `adv_balance` = `adv_balance` + :value WHERE `id` = :id")
            ->bindParam(':id', $id)
            ->bindParam(':value', $value)
            ->execute();
    }

    // перенаправить пользователя по адресу
    public static function  redirect($url)
    {

       header("Location:".$url);

    }


    // начисление денег за клик по баннеру
    public static function payForBannerClick($value){

        $login = User::getLogin();

        $user_db_table = USER_DB_TABLE;
        Yii::$app->db->createCommand("UPDATE $user_db_table
                                            SET `balance`=`balance`+:value
                                          WHERE `login`=:login")
                                    ->bindParam(':value', $value)
                                    ->bindParam(':login', $login)
                                    ->execute();

    }

    // снятие денег за клик по баннеру с автора баннера
    public static function author_MinusCost_For_Click($banner_author, $banner_cost)
    {
        Yii::$app->db->createCommand("
                UPDATE `user`
                SET `adv_balance` = `adv_balance` - :banner_cost
                WHERE `login` = :banner_author
            ")
            ->bindParam(':banner_cost', $banner_cost)
            ->bindParam(':banner_author', $banner_author)
            ->execute();
    }

    public static function checkLogin($login)
    {
        $login = User::find()->where(['login' => $login])->exists();
        return $login;
    }
	
	public static function changePoints($points){
		$pointsNew = Request::post('pointsNew');
	}
	
	public static function updatePoints($login, $value)
    {
		$login = User::getLogin();
        $user_db_table = USER_DB_TABLE;
        Yii::$app->db->createCommand("UPDATE $user_db_table SET `points` = `points` + :value WHERE `login` = :login")
            ->bindParam('login', $login)
            ->bindParam('value', $value)
            ->execute();
    }


    public static function getReferrals($id)
    {

    }

    public static function getRefovodId()
    {
        $id = Yii::$app->user->getId();
        $refovod_id = User::find()->where(['id' => User::getUserId()])->all();

        if (empty($refovod_id[0]["id_ref"]))
        {
            //$ref_id = 'У Вас нет рефовода';
            $ref_id = null;
        }
        else
        {
            $ref_id = $refovod_id[0]["id_ref"];
        }

        return $ref_id;

    }

    public static function getRefovodIdByID($id_user)
    {
        $id = Yii::$app->user->getId();
        $refovod_id = User::find()->where(['id' => $id_user])->all();

        if (empty($refovod_id[0]["id_ref"]))
        {
            //$ref_id = 'У Вас нет рефовода';
            $ref_id = null;
        }
        else
        {
            $ref_id = $refovod_id[0]["id_ref"];
        }

        return $ref_id;

    }

    // оплатить тому, кто привёл юзера его проценты
    public static function payHisRefovodPoints($points)
    {
        $id_ref = User::getRefovodId();
        $points_ref = $points * PAY_REFOVOD_1_LEVEL;
        $my_id = User::getUserId();

            Yii::$app->db->createCommand("
                UPDATE `user`
                SET `points` = `points` + :points
                WHERE `id` = :id
        ")
                ->bindParam(':points', $points_ref)
                ->bindParam(':id', $id_ref)
                ->execute();

        Yii::$app->db->createCommand("
                UPDATE `user`
                SET `points_ref_1` = `points_ref_1` + :points
                WHERE `id` = :id
        ")
            ->bindParam(':points', $points_ref)
            ->bindParam(':id', $id_ref)
            ->execute();
    }

    public static function payHisRefovodBonus($bonus_sum)
    {
        $id_ref = User::getRefovodId();
        $bonus_sum_ref = $bonus_sum * PAY_REFOVOD_1_LEVEL;
        $my_id = User::getUserId();

        Yii::$app->db->createCommand("
                UPDATE `user`
                SET `balance` = `balance` + :bonus_sum_ref
                WHERE `id` = :id
        ")
            ->bindParam(':bonus_sum_ref', $bonus_sum_ref)
            ->bindParam(':id', $id_ref)
            ->execute();

       // оплата процентов рефоводу этого юзера
        Yii::$app->db->createCommand("
                UPDATE `user`
                SET `points_ref_1` = `points_ref_1` + :bonus_sum_ref
                WHERE `id` = :id
        ")
            ->bindParam(':bonus_sum_ref', $bonus_sum_ref)
            ->bindParam(':id', $id_ref)
            ->execute();
    }

    // разрешает доступ только определённым IP-адресам
    public static function accessDeniedByIP()
    {
        $ip_granted = ['46.46.79.44'];

        foreach ($ip_granted as $ip)
        {
            if (User::getIP() == $ip)
            {
                continue;
            }

            else
            {
                exit("У вас нет прав!");
            }
        }
    }

    // проверяет зашёл ли это админ со своего IP или кто-то другой
    public static function isAdmin_By_IP()
    {
        if (User::getIP() == ADMIN_IP)
        {
            return true;
        }
    }

    // сколько всего пользователей
    public static function getQty()
    {
        $qty = User::find()->count('login');
        return $qty;
    }

    // сколько новых пользователей за сегодня
    public static function getQtyNewToday()
    {

        $datetimeTodayBegin = date('Y-m-d 00:00:01');
        $datetimeTodayEnd = date('Y-m-d 23:59:59');

        $qty = User::find()->where(['between', 'datetime', $datetimeTodayBegin, $datetimeTodayEnd])->count('login');
        return $qty;
    }

    // возвращает последний логин из БД
    public static function getLastLogin()
    {
        $user = User::find()->limit(1)->orderBy('datetime DESC')->all();
        return $user[0]['login'];
    }

    // когда (сек, мин, час назад) зарегистрировался последний пользователь
    public static function get_LastUser_TimePassedFromReg()
    {
        $user = User::find()->limit(1)->orderBy('datetime DESC')->all();
        $sec = time() - $user[0]['time'];
        $min =  $sec / 60;
        $hours = (int) ($min / 60);
        if ( $hours > 1 ) {
            $time = (int)$hours . ' ч';
        }
        else
        {
            $time = (int)$min . ' мин';
        }

        return $time;
    }

    // возвращает значение в колонке ref_level_1 т.е. сколько юзер заработал на своём реферале 1 уровня
    public static function getEarns_On_Referrals_1_Level()
    {
        $user_array = User::find()->where(['id' => User::getUserId()])->all();

        if (!empty($user_array))
        {
            $u_array = $user_array[0]['points_ref_1'];
        }
        else
        {
            $u_array = 0;
        }
        return $u_array;
    }

    // кол-во "моих" рефералов
    public static function getReferralsQty()
    {
        $user_array = User::find()->where(['id_ref' => User::getUserId()])->count();

        if ($user_array)
        {
            return $user_array;
        }
        else
        {
            return 0;
        }

    }

    // вывести логины "моих" рефов
	public static function getMyRefsLogins()
	{
		$logins_array = User::find()->where(['id_ref' => User::getUserId()])->orderBy('time ASC')->all();
		return $logins_array;
	}

	// вывести логины тех юзеров, у кого есть рефералы (т.е. колонка id_ref заполнена)
	public static function getWhoHasRefs()
	{
		$id_ref_count= User::find()->select('id_ref')->distinct()->count();
		return ($id_ref_count - 1);
	}

	public static function getLoginsWhoHasRefs()
	{
		$logins_who_has_refs_count= User::find()->where(['<>','id_ref','NULL'])->distinct()->all();
		return $logins_who_has_refs_count;
	}

	// подсчитать кол-во юзеров с рекл.балансом > 0 т.е. реклов
	public static function getAdvBalanceMoreZero()
    {
        $user_array = User::find()->where(['>', 'adv_balance', '0'])->all();
        return $user_array;
    }

    public static function checkExistsIP()
    {
        //$id_user = User::getUserId();
        $ip = User::getIP();
        $user_exists = User::find()->where(['ip' => $ip])->exists();
        return $user_exists;
    }

    //заполнить колонку IP которой раньше не было в БД
    public static function setIP()
    {
        $ip = User::getIP();
        $id_user = User::getUserId();

        if (!self::checkExistsIP())
        {
            Yii::$app->db->createCommand("
                UPDATE `user`
                SET `ip` = :ip
                WHERE `id` = :id_user
            ")
                ->bindParam(':ip', $ip)
                ->bindParam(':id_user', $id_user)
                ->execute();
        }
    }

    // подсчитать кол-во юзеров у которых уже заполнена колонка IP
    public static function qtyIP()
    {
        $qty_ip = User::find()->where(['<>', 'ip', 0])->count();
        return $qty_ip;
    }

    public static function getEarnTotal(){
		$earn_total = User::find()->sum('balance');
		return $earn_total;
    }

    public static function getEarnTotalOnRef(){
        $earn_total_on_refs = User::find()->where(['id' => User::getUserId()])->sum('points_ref_1');
        return $earn_total_on_refs;
    }

	public static function isAdminByIP(){
		if (User::getIP() != '46.46.79.44') {
			exit('Сайт временно на ремонте');
		}
	}


	public static function getMyRefsEarnTotal(){
        $my_id = (int)User::getUserId();
        $my_refs_earn_total = Bonus::find()->where(['id_ref' => $my_id])->sum('value');
        return $my_refs_earn_total;
    }

    public static function getMyRefsEarnOnPeriod($start, $end){
        $my_id = (int)User::getUserId();
        $my_refs_earn_total = Bonus::find()->where(['id_ref' => $my_id])->andWhere(['between', 'time', $start, $end])->sum('value');
        return $my_refs_earn_total;
    }

    public static function getRefs_ByUserID_EarnOnPeriod($idUser, $start, $end){
       $my_refs_earn_total = Bonus::find()
            ->where(['id_ref' => $idUser])
            ->andWhere(['between', 'time', $start, $end])
            ->sum('value');
            //->orderBy(['desc']);
        //$my_id = (int)User::getUserId();


        if (empty($my_refs_earn_total)) {
            return;
        } else {
            return $my_refs_earn_total;
        }


    }

    public static function getLoginOnID($id){
        $user_array = User::findOne($id);
        return $user_array['login'];
    }

    public static function getTotalQTY(){
        return User::find()->count('id');
    }

    public static function getQtyNewUsersToday()
    {

        $beginOfDay = strtotime("today");
        $endOfDay = strtotime("tomorrow");

        $qty = User::find()->where(['between', 'time', $beginOfDay, $endOfDay])->count('login');
        return $qty;
    }

    public static function getQtyNewUsersYesterday()
    {

        $beginOfDay = strtotime("yesterday");
        $endOfDay = strtotime("today");

        $qty = User::find()->where(['between', 'time', $beginOfDay, $endOfDay])->count('login');
        return $qty;
    }

    public static function getHowManyUsersHaveRefs(){
        $user_array = User::find()->select('id_ref')->distinct()->count(0);
        return $user_array;
    }

    public static function getEarnOnRefs($id){
        $res = User::find()->all();
        return $res;
    }

    public static function getMyRefs(){
        $id = User::getUserId();
        $my_refs = User::find()->where(['id_ref' => $id])->all();
        return $my_refs;
    }

    public static function getMyFirstRefs($count){
        $id = User::getUserId();
        $my_refs = User::find()->where(['id_ref' => $id])->orderBy('balance')->limit(10)->all();
        return $my_refs;
    }

    public static function getEarnsOnRef($id){
        $user_array = User::find()->where(['id' => User::getUserId()])->one();
        return $user_array['points_ref_1'];
    }

    public static function isBanned($id){
        $ban = User::find()
            ->where(['id' => User::getUserId()])
            ->andWhere(['ban' => 'frozen'])
            ->orWhere(['ban' => 'ban'])
            ->all();
        return $ban;
    }

}
